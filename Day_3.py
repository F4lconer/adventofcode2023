"""
https://adventofcode.com/2023/day/3

--- Day 3: Gear Ratios ---

You and the Elf eventually reach a gondola lift station; he says the gondola lift will take you up to the water source,
but this is as far as he can bring you. You go inside.

It doesn't take long to find the gondolas, but there seems to be a problem: they're not moving.

"Aaah!"

You turn around to see a slightly-greasy Elf with a wrench and a look of surprise. "Sorry, I wasn't expecting anyone!
The gondola lift isn't working right now; it'll still be a while before I can fix it." You offer to help.

The engineer explains that an engine part seems to be missing from the engine, but nobody can figure out which one.
f you can add up all the part numbers in the engine schematic, it should be easy to work out which part is missing.

The engine schematic (your puzzle input) consists of a visual representation of the engine. There are lots of numbers
and symbols you don't really understand, but apparently any number adjacent to a symbol, even diagonally, is a
"part number" and should be included in your sum. (Periods (.) do not count as a symbol.)

Here is an example engine schematic:

467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..

In this schematic, two numbers are not part numbers because they are not adjacent to a symbol: 114 (top right)
and 58 (middle right). Every other number is adjacent to a symbol and so is a part number; their sum is 4361.

Of course, the actual engine schematic is much larger. What is the sum of all of the part numbers in the engine schematic?

--- Part Two ---

The engineer finds the missing part and installs it in the engine! As the engine springs to life, you jump in the
closest gondola, finally ready to ascend to the water source.

You don't seem to be going very fast, though. Maybe something is still wrong? Fortunately, the gondola has a phone
labeled "help", so you pick it up and the engineer answers.

Before you can explain the situation, she suggests that you look out the window. There stands the engineer, holding
a phone in one hand and waving with the other. You're going so slowly that you haven't even left the station.
You exit the gondola.

The missing part wasn't the only issue - one of the gears in the engine is wrong. A gear is any * symbol that is
adjacent to exactly two part numbers. Its gear ratio is the result of multiplying those two numbers together.

This time, you need to find the gear ratio of every gear and add them all up so that the engineer can figure out which
gear needs to be replaced.

Consider the same engine schematic again:

467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..

In this schematic, there are two gears. The first is in the top left; it has part numbers 467 and 35,
so its gear ratio is 16345. The second gear is in the lower right; its gear ratio is 451490.
(The * adjacent to 617 is not a gear because it is only adjacent to one part number.)
Adding up all of the gear ratios produces 467835.

"""
from pprint import pprint
from typing import Optional


def get_numbers_from_line(line: str) -> list[tuple[int, int, int]]:
    numbers: list[tuple[int, int, int]] = []
    number_str: str = ""
    start_index: int = -1

    for i, character in enumerate(line):
        if character.isdigit():
            if start_index == -1:
                start_index = i
            number_str += character
        else:
            if number_str:
                end_index: int = i - 1
                numbers.append((start_index, end_index, int(number_str)))

                start_index = -1
                number_str = ""
    return numbers


def check_character(character: str) -> Optional[str]:
    if not character.isdigit():
        return character
    else:
        return None


def check_surrounding_characters(row: int, part: tuple[int, int, int], plan: list[list[str]],
                                 filtered_char: Optional[str] = None) -> list[tuple[int, int, str]]:
    start_index, end_index, number = part
    result = []
    for i in range(start_index, end_index + 1):
        symbol = ''
        # row above
        if row > 0:
            symbol = check_character(plan[row - 1][i])
            if symbol:
                result.append((row, i, symbol))
        # row below
        if row < len(plan) - 1:
            symbol = check_character(plan[row + 1][i])
            if symbol:
                result.append((row, i, symbol))

    # left & left diagonal
    if start_index > 0:
        # one left
        symbol = check_character(plan[row][start_index - 1])
        if symbol:
            result.append((row, start_index - 1, symbol))
        # diagonal left top
        if row > 0:
            symbol = check_character(plan[row - 1][start_index - 1])
            if symbol:
                result.append((row - 1, start_index - 1, symbol))
        # diagonal left bottom
        if row < len(plan) - 1:
            symbol = check_character(plan[row + 1][start_index - 1])
            if symbol:
                result.append((row + 1, start_index - 1, symbol))

    # right & right diagonal
    if end_index < len(plan[row]) - 1:
        # one right
        symbol = check_character(plan[row][end_index + 1])
        if symbol:
            result.append((row, end_index + 1, symbol))
        # diagonal right top
        if row > 0:
            symbol = check_character(plan[row - 1][end_index + 1])
            if symbol:
                result.append((row - 1, end_index + 1, symbol))
        # diagonal right bottom
        if row < len(plan) - 1:
            symbol = check_character(plan[row + 1][end_index + 1])
            if symbol:
                result.append((row + 1, end_index + 1, symbol))

    # filter out dot characters from the list
    filtered_dot = '.'
    result = [filtered for filtered in result if filtered[2] != filtered_dot]
    if filtered_char:
        result = [filtered for filtered in result if filtered[2] == filtered_char]

    return result


if __name__ == "__main__":
    engine_parts: list[list[tuple[int, int, int]]] = []
    engine_plan: list[list[str]] = []

    # Parse the input data
    with open("inputs/day_3_input_sample", "r") as f:
        for line in f:
            engine_parts.append(get_numbers_from_line(line))
            engine_plan.append(list(line.strip()))

    # visualize
    print("Parts:")
    pprint(engine_parts)
    print("\nPlan:")
    pprint(engine_plan)

    print(f"\n\n{' Validation ':=^30}")
    # check surrounding characters
    good_parts: list[int] = []
    shared_cog_numbers: list[int] = []
    parts_with_cogs: list[tuple[int, int, int], list[tuple[int, int, str]]] = []
    for row, parts in enumerate(engine_parts):
        for part in parts:
            print(f"Row: {row}, Part: {part}")

            parts_with_cogs.append(check_surrounding_characters(row, part, engine_plan, filtered_char='*'))

            # TODO: Continue here sort out which parts are sharing the asterisk symbol

    print(f"{' DEBUG ':=^30}")
    print(parts_with_cogs)

    print(f"{' Good parts ':=^30}")
    print(good_parts)
    print(f"Sum of good parts [part 1 answer]: {sum(good_parts)}")
    print()
    print()
    print(f"{' PART TWO ':=^30}")
    # Part 2
    good_parts = [part for part in good_parts]

    print(parts_with_cogs)
    print()

    for row, parts in enumerate(engine_parts):
        for part in parts:
            ...
    print(f"Remaining parts [part 2 answer]: TODO RESPONSE HERE")
