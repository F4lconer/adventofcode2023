"""
https://adventofcode.com/2023/day/1
--- Day 1: Trebuchet?! ---

Something is wrong with global snow production, and you"ve been selected to take a look.
The Elves have even given you a map; on it, they"ve used stars to mark the top fifty locations that are likely to be having problems.

You"ve been doing this long enough to know that to restore snow operations, you need to check all fifty stars
by December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar;
the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!

You try to ask why they can"t just use a weather machine ("not powerful enough") and where they"re even sending you
("the sky") and why your map looks mostly blank ("you sure ask a lot of questions") and hang on did you just say the sky
("of course, where do you think snow comes from") when you realize that the Elves are already loading you into a
trebuchet ("please hold still, we need to strap you in").

As they"re making the final adjustments, they discover that their calibration document (your puzzle input) has been
amended by a very young Elf who was apparently just excited to show off her art skills.
Consequently, the Elves are having trouble reading the values on the document.

The newly-improved calibration document consists of lines of text; each line originally contained a specific
calibration value that the Elves now need to recover. On each line, the calibration value can be found by combining
the first digit and the last digit (in that order) to form a single two-digit number.

For example:
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet

In this example, the calibration values of these four lines are 12, 38, 15, and 77. Adding these together produces 142.

Consider your entire calibration document. What is the sum of all of the calibration values?

--- Part Two ---

Your calculation isn't quite right. It looks like some of the digits are actually spelled out with letters:
one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".

Equipped with this new information, you now need to find the real first and last digit on each line. For example:

two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen

In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76. Adding these together produces 281.

"""
digits = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}


def convert_num_to_digits(text: str):
    """
    Convert numbers in text to digits
    :param text: text to convert
    :return: converted text
    """
    if not text:
        return ""
    for word, digit in digits.items():
        if text.startswith(word):
            return f"{digit}{convert_num_to_digits(text[len(word)-1:])}"
        elif text.startswith(digit):
            return f"{digit}{convert_num_to_digits(text[len(digit):])}"
    return convert_num_to_digits(text[1:])


if __name__ == "__main__":
    # map to digits in words

    # print(convert_num_to_digits("eightwothree"))

    with open("inputs/day_1_input", "r") as f:
        total_sum = 0
        for line in f:
            print(line.strip())
            only_digits = convert_num_to_digits(line)
            print(only_digits)
            # get first and last digit, convert to full number and add to total sum
            print(int(f"{only_digits[0]}{only_digits[-1]}"))
            print()
            total_sum += int(f"{only_digits[0]}{only_digits[-1]}")
        print(total_sum)
